package user

import (
	"context"
	"go-api-skeleton/entity"
	"go-api-skeleton/model"
)

type Usecase interface {
	VerifikasiToken(ctx context.Context, token string) (codes int, vrf model.VerifikasiToken, err error)
	InsertUser(ctx context.Context, user entity.Users) (err error)
	SendErrorToTelegram(nameService, message string)
	GetOneUser(ctx context.Context) (user entity.Users, err error)
	GetAllUsers(ctx context.Context) (users []entity.Users, err error)
	UpdateUserByID(ctx context.Context, id int, fullname string) (err error)
}

type ToolsUsecase interface {
	SendEmails(from, pass, to, identity, msg, smtpMail, port string) (err error)
}

type Repository interface {
	InsertUser(ctx context.Context, user entity.Users) (err error)
	GetOneUser(ctx context.Context) (user entity.Users, err error)
	GetAllUsers(ctx context.Context) (users []entity.Users, err error)
	UpdateUserByID(ctx context.Context, id int, fullname string) (err error)
}

type API interface {
	VerifikasiToken(ctx context.Context, token string) (codes int, vrf model.VerifikasiToken, err error)
	SendErrorToTelegram(nameService, message string)
}

type Worker interface {
}

type Tools interface {
}
