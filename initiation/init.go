package initiation

import (
	"database/sql"
	"go-api-skeleton/config"
	"go-api-skeleton/handler"
	"go-api-skeleton/handler/user"
	"go-api-skeleton/helper"
	route "go-api-skeleton/service"
	userRepo "go-api-skeleton/service/repository/user/postgresql"
	userUsecase "go-api-skeleton/service/usecase/user"
	"os"

	"go-api-skeleton/service/api"
	"go-api-skeleton/service/tools"
	"go-api-skeleton/service/worker"

	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis/v8"
)

func Init(routesGin *gin.Engine, logger *helper.CostumLogger) (*gin.Engine, *sql.DB, *redis.Client) {
	//bisa nambah config db lain
	db := config.Postgresql(logger) // settingan db postgresql

	//dbRead := config.Postgresql(logger) // settingan dbRead postgresql
	//dbWrite := config.Postgresql(logger) // settingan dbWrite postgresql

	redis := config.Redis(logger)

	repo := userRepo.CreateRepository(db, db, logger) // Create transaction from db
	apis := api.CreateApi(redis, logger)
	workers := worker.CreateWorker(logger)
	tools := tools.CreateTools(logger)
	userUsecase := userUsecase.CreateUsecase(repo, apis, workers, tools, logger)

	response := helper.CreateCustomResponses(os.Getenv("DOMAIN_NAME"))

	//init JWT
	initJwt := handler.InitJwt(logger, redis, userUsecase, response)
	userHandlers := user.CreateHandler(userUsecase, redis, logger, response) // Assign function repository for using on handler

	r := route.Routes(routesGin, initJwt, userHandlers)
	return r, db, redis
}
