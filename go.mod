module go-api-skeleton

go 1.15

require (
	github.com/garyburd/redigo v1.6.2
	github.com/gin-contrib/pprof v1.3.0
	github.com/gin-gonic/gin v1.7.1
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/go-redis/redis/v8 v8.10.0
	github.com/go-sql-driver/mysql v1.6.0
	github.com/joho/godotenv v1.3.0
	github.com/lib/pq v1.10.1
	github.com/olivere/elastic/v7 v7.0.25
	github.com/sirupsen/logrus v1.8.1
	go.elastic.co/apm/module/apmgin v1.12.0
	go.elastic.co/apm/module/apmgoredisv8 v1.12.0
	go.elastic.co/apm/module/apmhttp v1.12.0
	go.elastic.co/apm/module/apmlogrus v1.12.0
	go.elastic.co/apm/module/apmsql v1.12.0
	gopkg.in/sohlich/elogrus.v7 v7.0.0
	gorm.io/driver/mysql v1.0.6
	gorm.io/gorm v1.21.9
)
