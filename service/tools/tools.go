package tools

import (
	"go-api-skeleton/helper"
	"net/smtp"
)

type Tools struct {
	Logger *helper.CostumLogger
}

func CreateTools(logger *helper.CostumLogger) *Tools {
	return &Tools{logger}
}

func (a *Tools) SendEmails(from, pass, to, identity, msg, smtpMail, port string) (err error) {
	err = smtp.SendMail(smtpMail+":"+port,
		smtp.PlainAuth(identity, from, pass, smtpMail),
		from, []string{to}, []byte(msg))
	if err != nil {
		a.Logger.Error(err.Error())
		return
	}
	return
}
