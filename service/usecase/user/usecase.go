package user

import (
	"context"
	"go-api-skeleton/entity"
	"go-api-skeleton/helper"
	"go-api-skeleton/interfaces/user"
	"go-api-skeleton/model"
)

// Usecase is
type Usecase struct {
	Repository user.Repository
	Api        user.API
	Worker     user.Worker
	Tools      user.Tools
	Logger     *helper.CostumLogger
}

// CreateUsecase is
func CreateUsecase(repo user.Repository, api user.API, worker user.Worker, tools user.Tools, logger *helper.CostumLogger) *Usecase {
	return &Usecase{
		Repository: repo,
		Api:        api,
		Worker:     worker,
		Tools:      tools,
		Logger:     logger,
	}
}

func (b Usecase) VerifikasiToken(ctx context.Context, token string) (codes int, vrf model.VerifikasiToken, err error) {
	return b.Api.VerifikasiToken(ctx, token)
}

func (b Usecase) InsertUser(ctx context.Context, user entity.Users) (err error) {
	return b.Repository.InsertUser(ctx, user)
}

func (b Usecase) SendErrorToTelegram(nameService, message string) {
	b.Api.SendErrorToTelegram(nameService, message)
}

func (b Usecase) GetOneUser(ctx context.Context) (user entity.Users, err error) {

	return b.Repository.GetOneUser(ctx)
}

func (b Usecase) GetAllUsers(ctx context.Context) (users []entity.Users, err error) {
	return b.Repository.GetAllUsers(ctx)
}

func (b Usecase) UpdateUserByID(ctx context.Context, id int, fullname string) (err error) {
	return b.Repository.UpdateUserByID(ctx, id, fullname)
}
