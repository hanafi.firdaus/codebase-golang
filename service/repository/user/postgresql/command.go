package postgresql

import (
	"context"
	"database/sql"
	entity "go-api-skeleton/entity"
	"go-api-skeleton/helper"
)

type Repository struct {
	// CQRS design pattern best practice. memisahkan DB untuk write dan read
	DBWrite     *sql.DB
	DBRead     *sql.DB
	logger *helper.CostumLogger
}

func CreateRepository(dbWrite, dbRead *sql.DB, logger *helper.CostumLogger) *Repository {
	return &Repository{dbWrite, dbRead, logger}
}

func (r Repository) InsertUser(ctx context.Context, user entity.Users) (err error) {
	query := "INSERT INTO sharing_session.users(fullname, no_hp, is_attend) values ($1, $2, $3)"

	_, err = r.DBWrite.ExecContext(ctx, query, user.Fullname, user.NoHP, user.IsAttend)
	if err != nil {
		return
	}
	return
}

func (r Repository) UpdateUserByID(ctx context.Context, id int, fullname string) (err error) {
	query := "UPDATE sharing_session.users SET fullname = $1 where id = $2"
	_, err = r.DBWrite.ExecContext(ctx, query, fullname, id)
	if err != nil {
		return
	}

	return
}
