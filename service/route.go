package service

import (
	"go-api-skeleton/handler"
	"go-api-skeleton/handler/user"
	"os"

	"github.com/gin-gonic/gin"
)

// Routes is
func Routes(routesGin *gin.Engine, jwt *handler.JWT, hand *user.Handler) *gin.Engine {
	version := os.Getenv("VERSION_API")

	// Grouping path api
	jwtAuth := routesGin.Group("/" + version + "/api")

	// api with verifikasi jwt token
	jwtAuth.GET("/userall", hand.GetAllUsers)

	jwtAuth.Use(jwt.VerifyAutorizationToken()) // Verify Authorization
	{
		jwtAuth.GET("/test", hand.Test)
		jwtAuth.POST("/user", hand.InsertUser)
		jwtAuth.GET("/user/one", hand.GetOneUser)
		jwtAuth.PUT("/user/:id", hand.UpdateFullnameUserByID)
	}

	return routesGin
}
